# GEODUCK

### Install
```
npm install
```

### Create a `config.json` like so:

```
{
  "farmOSURI": "https://[[my_farmos_instance]].farmos.net/farm/sensor/listener/[[public_key]]?private_key=[[private_key]]",
  "port": 5000, ==> defaults to 3000
  "timeout": 25 ==> timeout interval on requests to FarmOS in seconds, defaults to 30
}
```

### Start with:
```
npm start
```

### Post data:
Data can be posted as JSON to:
```
http://localhost:{PORT}/
```

With header:
```
'Content-Type: application/json'
```

Curl example (on port 3000):
```
curl -X POST \
  http://localhost:3000 \
  -H 'Content-Type: application/json' \
  -d '{"my_sensor": 123}'
```

Data posted should be a JSON object like:
```
{ "[[sensor_name]]": [[sensor_value]],  "[[second_sensor_name]]": [[second_sensor_value]], ...}
```

### Debugging
To see additional logging, start with:
```
npm run start-debug
```

Which is shorthand for:
```
DEBUG=geoduck npm start
```
