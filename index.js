const express = require('express')
const bodyParser = require('body-parser')
const debug = require('debug')('geoduck');

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const formatData = require('./helpers/formatData');
const Queue = require('./Queue');

const app = express()

const adapter = new FileSync('db.json')
const db = low(adapter)
const lodashId = require('lodash-id')
db._.mixin(lodashId)

var config;
try{
  config = require('./config.json')
}catch(err){
  console.warn('config.json not set up!')
}

const port = config.port || 3000;

db.defaults({ history: [] })
  .write()

const queue = new Queue(
  // getFromDbById
  (entryId)=> {
    return db
      .get('history')
      .getById(entryId)
      .cloneDeep()
      .value()
  },
  // markAsUploadedInDb
  (entryId)=> {
    db.get('history')
      .getById(entryId)
      .assign({ uploaded: true})
      .write()
  },
  // farmOSURI
  config.farmOSURI,
  // timeoutInterval
  config.timeout
);

const backValues = db.get('history')
  .filter({uploaded: false})
  .map('id')
  .value()

backValues.forEach((id) => {
  queue.push(id)
})


app.use(bodyParser.json())

app.post('/', (req, res) => {
  const formattedData = formatData(req.body);
  const entry = db.get('history')
    .insert(formattedData)
    .write()

  queue.push(entry.id);
  res.sendStatus(200);
  // TODO: add error handling
})



app.listen(port, () => console.log(`Geoduck listening on port ${port}!`))
